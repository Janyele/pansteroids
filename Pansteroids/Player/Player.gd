extends KinematicBody2D

var rforce:float = 0.0
var lforce:float = 0.0
var speed:float = 0.0
onready var bulletcont:Node2D = get_parent().get_node("Bullets")

func _ready() -> void:
	pass # Replace with function body.

func _process(delta) -> void:
	if Input.is_action_pressed("ui_right"):
		rforce = clamp(rforce + delta/10, 0.0, 0.04)
	else:
		rforce = clamp(rforce - delta/25, 0.0, 0.04)
	if Input.is_action_pressed("ui_left"):
		lforce = clamp(lforce + delta/10, 0.0, 0.04)
	else:
		lforce = clamp(lforce - delta/25, 0.0, 0.04)
	if Input.is_action_pressed("ui_up"):
		speed = clamp(speed + delta*100, 0.0, 100.0)
	else:
		speed = clamp(speed - delta*50, 0.0, 100.0)
	rotation += rforce - lforce
	move_and_slide((position.direction_to($Direction.global_position))*speed)

func _input(event) -> void:
	if event.is_action_pressed("ui_select"):
		for i in 3:
			var bullet = get_node("Direction/Bullet" + str(i+1))
			var bulltmp = bullet.duplicate()
			bulltmp.position = bullet.global_position
			bulltmp.rotation = bullet.global_rotation
			bulltmp.canmove = true
			bulletcont.add_child(bulltmp)

extends Node2D

func _ready() -> void:
	randomize()
	for i in 600:
		var tmp_star = $BaseStar.duplicate()
		tmp_star.rect_position = Vector2(rand_range(0,1024),rand_range(0,600))
		tmp_star.rect_size = Vector2.ONE * (1 + randi()%4)
		tmp_star.modulate.a = rand_range(0.4, 1.0)
		tmp_star.rect_rotation = randi() % 360
		add_child(tmp_star)
	
	#Experimental Space
	for i in 1500:
		var tmp_space = $BlueSpace.duplicate()
		tmp_space.rect_position = Vector2(rand_range(0,1024),rand_range(0,600))
		tmp_space.rect_size = Vector2.ONE * (10 + randi()%20)
		tmp_space.modulate.a = rand_range(0.03, 0.12)
		tmp_space.rect_rotation = randi() % 360
		add_child(tmp_space)
	for i in 1500:
		var tmp_space = $YellowSpace.duplicate()
		tmp_space.rect_position = Vector2(rand_range(0,1024),rand_range(0,600))
		tmp_space.rect_size = Vector2.ONE * (10 + randi()%35)
		tmp_space.modulate.a = rand_range(0.03, 0.12)
		tmp_space.rect_rotation = randi() % 360
		add_child(tmp_space)

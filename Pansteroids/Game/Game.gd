extends Node2D

onready var pathfollow:PathFollow2D = $Path2D/PathFollow2D
onready var pansteroid:PackedScene = preload("res://Objetcs/Pansteroid/Pansteroid.tscn")

func _ready() -> void:
	pass

func get_spawn_pos() -> Vector2:
	randomize()
	pathfollow.unit_offset = rand_range(0.0, 1.0)
	return pathfollow.position

func _add_pansteroid() -> void:
	var tmp_pan = pansteroid.instance()
	add_child(tmp_pan)

extends Node2D

export (bool) var canmove:bool = true

func _ready() -> void:
	if canmove:
		$Timer.start()
	set_process(canmove)

func _process(delta) -> void:
	move_local_y(delta * -450)

func _on_Area2D_body_entered(body:RigidBody2D) -> void:
	if body:
		body.set_danger(35)
		queue_free()

func _on_Timer_timeout() -> void:
	queue_free()

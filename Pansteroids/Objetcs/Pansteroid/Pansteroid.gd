extends RigidBody2D

onready var parent:Node2D = get_parent()
var panstat:int = 100

func _ready() -> void:
	randomize()
	bounce = 1
	position = parent.get_spawn_pos()

	var numpan:int = 1 + randi() % 5
	var pantxt:Texture = load("res://Resources/Panspr/Pan"+str(numpan)+".png")
	$Sprite.texture = pantxt

	var sizey:float = $Sprite.texture.get_size().y
	$Sprite.scale = Vector2.ONE * 150 / sizey

	angular_velocity = rand_range(-1,1)
	linear_velocity = position.direction_to(Vector2(512,300)) * 150

func _process(delta:float) -> void:
	if linear_velocity.length() < 100:
		linear_velocity += linear_velocity

func set_danger(danger:int) -> void:
	panstat -= danger
	if panstat <= 0:
		queue_free()
